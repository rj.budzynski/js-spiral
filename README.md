# Ulam's spiral

See [Wikipedia](https://en.wikipedia.org/wiki/Ulam_spiral).

[See it live](https://rjb59071.surge.sh/).

There are two implementations here. One that uses HTML Canvas, and another that
builds the picture out of a zillion HTML elements (I used \<p>, because the
tag name is a single character) with backgrounds set by CSS.

The progressive rendering is just eye candy, sort of.

Comments to [Robert@Budzynski.xyz](mailto:Robert@Budzynski.xyz).