const $ = document.querySelector.bind(document);

class MyCanvas {

    constructor(selector) {
        this.canvas = $(selector);
        let width = this.canvas.clientWidth, height = this.canvas.clientHeight;
        this.canvas.setAttribute('width', parseInt(width));
        this.canvas.setAttribute('height', parseInt(height));
        this.gc = this.canvas.getContext('2d');
        this.gc.lineWidth = 2;
        this.gc.strokeStyle = 'black';
    }

    clear(fillStyle = 'transparent') {
        this.gc.fillStyle = fillStyle;
        this.gc.fillRect(0, 0, this.canvas.width, this.canvas.height);
    }

    *drawSpiral() {
        this.numbers = [];
        this.gc.fillStyle = this.gc.strokeStyle;
        const delta = this.gc.lineWidth;
        const UP = [0, -delta],
            RIGHT = [delta, 0],
            DOWN = [0, delta],
            LEFT = [-delta, 0];
        const DIRECTIONS = [RIGHT, UP, LEFT, DOWN];
        const [centerX, centerY] = [(this.canvas.width / 2) | 0, (this.canvas.height / 2) | 0];
        let x = centerX, y = centerY;
        let k = 0, N = 0;
        let nprimes = 0;
        while (x > 0 && x < this.canvas.width - 1 && y > 0 && y < this.canvas.height - 1) {
            let v = DIRECTIONS[k % 4];
            for (let j = 0; j < (k + 1) / 2 | 0; ++j) {
                if (isPrime(++N)) {
                    ++nprimes;
                    this.gc.fillRect(x - 1, y - 1, delta, delta);
                }
                if (this.numbers[y] === undefined) {
                    this.numbers[y] = [];
                }
                this.numbers[y][x] = N;
                x += v[0];
                y += v[1];
            }
            ++k;
            yield N;
        }
        this.nprimes = nprimes;
    }
}

let canvas0;

window.addEventListener('load', () => {
    canvas0 = new MyCanvas('#canvas0');
    canvas0.clear();
    canvas0.draw = canvas0.drawSpiral();
    canvas0.intervalId = window.setInterval(() => {
        let { value, done } = canvas0.draw.next();
        if (done) {
            window.clearInterval(canvas0.intervalId);
            $('#info').innerHTML = `<h3>Ulam's spiral</h3>
            N = ${canvas0.N}<br>
            # of primes = ${canvas0.nprimes}<br>
            drawn with HTML Canvas<br>
            <a href=index1.html target=_blank>non-canvas version</a>`;
            $('#info').style.display = 'block';
        }
        canvas0.N = value;
    }, 15);
    canvas0.canvas.addEventListener('mousemove', (ev) => {
        let rect = canvas0.canvas.getBoundingClientRect();
        let y = (ev.clientY - rect.y) | 0;
        let row = canvas0.numbers[y] || canvas0.numbers[y-1];
        if (row === undefined) {
            canvas0.canvas.title = '';
            return;
        }
        let x = (ev.clientX - rect.x) | 0;
        let N = row[x] || row[x-1] || '';
        canvas0.canvas.title = N;
    })
});