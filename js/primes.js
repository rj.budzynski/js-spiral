const isPrime = (() => {
    const primes = [2, 3];
    return (n) => {
        if (primes.includes(n)) return true;
        if (n < primes[primes.length - 1]) return false;
        let p = primes[primes.length - 1] + 2;
        loop0:
        while (p <= n) {
            for (k of primes) {
                if (p < k*k) break;
                if (p % k == 0) {
                    p += 2;
                    continue loop0;
                }
            }
            primes.push(p);
            p += 2;
        }
        return primes.includes(n);
    };
})();