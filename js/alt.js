const $ = document.querySelector.bind(document);
const cE = document.createElement.bind(document);
const tiles = [];

function makeBoard(containerSelector, delta) {

    const container = $(containerSelector);
    let cstyle = getComputedStyle(container);
    let width = parseInt(cstyle.width);
    let height = parseInt(cstyle.height);
    container.style.width = `${width}px`;
    container.style.height = `${height}px`;
    let cols = width / delta |0,
        rows = height / delta |0;
    for (let i = 0; i < rows; ++i) {
        let row = [];
        for (let k = 0; k < cols; ++k) {
            let tile = cE('p');
            tile.style.width = `${delta}px`;
            tile.style.height = `${delta}px`;
            container.appendChild(tile);
            row.push(tile);
        }
        tiles.push(row);
    }
}

function* makeSpiral(tiles) {
    const UP = [0, -1],
        RIGHT = [1, 0],
        DOWN = [0, 1],
        LEFT = [-1, 0];
    const DIRECTIONS = [RIGHT, UP, LEFT, DOWN];
    let h = tiles.length,
        w = tiles[0].length;
    let x = (w / 2) | 0, y = (h / 2) | 0;
    let k = 0, N = 0;
    let nprimes = 0;
    while (x >= 0 && x < w && y >= 0 && y < h) {
        let v = DIRECTIONS[k % 4];
        for (let j = 0; j < ((k + 1) / 2) | 0; ++j) {
            if (isPrime(++N)) {
                tiles[y][x].classList.add('p');
                ++nprimes;
            }
            tiles[y][x].title = N;
            x += v[0];
            y += v[1];
        }
        ++k;
        yield N;
    }
    tiles.nprimes = nprimes;
}

window.addEventListener('load', () => {
    makeBoard('#board', 2);
    $('#board').draw = makeSpiral(tiles);
    $('#board').intervalId = window.setInterval(() => {
        let { value, done } = $('#board').draw.next();
        if (done) {
            window.clearInterval($('#board').intervalId);
            $('#info').innerHTML = `<h3>Ulam's spiral</h3>
            N = ${tiles.N}<br>
            # of primes = ${tiles.nprimes}<br>
            drawn with HTML/CSS<br>
            <a href=index.html target=_blank>HTML Canvas version</a>`;
            $('#info').style.display = 'block';
        }
        tiles.N = value;
    }, 15);
});